# Requêtes de la BDD étudiants

## 1. Sélection de données

1. Toute la table etudiants :
```
SELECT * FROM etudiants;
```

2. Nom, numéro et date de naissance des étudiants :
```
SELECT nom_etu,
	num_etu,
	date_naiss
FROM etudiants;
```

3. Liste des étudiantes :
```
SELECT *
FROM etudiants
WHERE sexe = 'F';
```

4. Liste des enseignants par ordre alphabétique des noms :
```
SELECT * 
FROM enseignants
ORDER BY nom_ens;
```

5. Liste des enseignants par grade et par ordre alphabétique décroissant des noms  :
```
SELECT grade, 
	nom_ens 
FROM enseignants
-- on peut trier par plusieurs colonnes --
-- DESC indique que l'on veut trier dans le sens inverse --
ORDER BY grade,
	nom_ens DESC;
```

6. Nom, grade et ancienneté des enseignants qui ont strictement plus de 2 ans d'ancienneté :
```
SELECT nom_ens,
	grade,
	anciennete
FROM enseignants
WHERE anciennete > 2;
```

7. Nom, grade et ancienneté des maîtres de conférences (MCF) qui ont 3 ans d'ancienneté ou plus :
```
SELECT nom_ens,
	grade,
	anciennete
FROM enseignants
WHERE grade = 'MCF' AND anciennete >= 3;
```

8. Nom et date de naissance des étudiants masculins nés après 1990 :
```
SELECT nom_etu,
	date_naiss
FROM etudiants
WHERE sexe = 'M' AND date_naiss > '1990-12-31';
```

9. Lignes de la table notes correspondant à une note inconnue :
```
SELECT * 
FROM notes
WHERE note IS NULL;
```

10. Nom des enseignants professeurs(PR) ou associés(ASS), en utilisant l'opérateur IN :
```
SELECT nom_ens
FROM enseignants
WHERE grade IN ('PR', 'ASS');
```

11. Nom des enseignants dont le nom ou le prénom contiennent un J :
```
SELECT nom_ens 
FROM enseignants
-- utilisation d'une regex pour gérer la casse --
WHERE nom_ens REGEXP '[Jj]';
```

12. Nom et date de naissance des étudiants nés en 1990 :
```
SELECT nom_etu,
	date_naiss
FROM etudiants
-- DATE_FORMAT([donnée de type date], [format]) = [date] -- 
-- '%Y' <=> année sous forme 4 chiffres -- 
-- ici, on évalue les données de type date de la coloone date_naiss --
WHERE DATE_FORMAT(date_naiss, '%Y') = '1990';
```

13. Nom et âge (en années) des étudiants de 23 ans ou plus : 
```
-- CURDATE() retourne la date acutelle --
-- YEAR() retourne l'année d'un objet DATE --
SELECT DISTINCT nom_etu, 
	YEAR(CURDATE()) - YEAR(date_naiss)  AS Age
FROM etudiants
WHERE YEAR(CURDATE()) - YEAR(date_naiss) >= 23;
```

<br>
<hr>
<br>

## 2. Jointures

1. Notes obtenues par l'étudiant Dupont, Charles :
```
SELECT _num_mat, 
	note
FROM notes
JOIN etudiants ON num_etu = _num_etu
WHERE nom_etu = 'Dupont, Charles';
```

2. Note obtenue par l'étudiant Dupont, Charles en 'G.P.A.O.' :
```
SELECT _num_mat, 
	note
FROM notes
JOIN etudiants ON num_etu = _num_etu
RIGHT JOIN matieres ON num_mat = _num_mat
WHERE nom_etu = 'Dupont, Charles' AND nom_mat = 'G.P.A.O.';
```

3. Nom et date de naissance des étudiants plus jeunes(en années) que l'étudiant Dupont, Charles :
```
SELECT nom_etu, 
	date_naiss
FROM etudiants
-- on précise bien que c'est l'année qui nous intéresse dans l'âge --
WHERE YEAR(CURDATE()) - YEAR(date_naiss) > (
	-- sous-requête pour déterminer l'âge de Charles --
	SELECT YEAR(CURDATE()) - YEAR(date_naiss)
	FROM etudiants
	WHERE nom_etu = 'Dupont, Charles'
); 
```
4. Nom des étudiants ayant eu la moyenne dans une des matières enseignées par Simon, Etienne :
```
-- DISTINCT car Etienne Simon enseigne peut-être plusieurs matières --
SELECT DISTINCT nom_etu
FROM etudiants
RIGHT JOIN notes ON num_etu = _num_etu`
RIGHT JOIN matieres ON num_mat = _num_mat
RIGHT JOIN enseignants ON num_ens = _num_ens
WHERE nom_ens = 'Simon, Etienne'
-- la note données pour une matière est la moyenne de l'étudiant dans cette matière --
AND note >= 10;
```

5. Nom des étudiants qui ont eu une note en "Logique" inférieure à celle de "Statistiques" :
```
SELECT DISTINCT nom_etu, 
	num_mat
FROM etudiants
RIGHT JOIN notes ON _num_etu = num_etu
RIGHT JOIN matieres ON num_mat = _num_mat
WHERE (matieres.nom_mat = 'Logique') < (matieres.nom_mat ='Statistiques');
```

6. Nom des étudiants ayant eu une plus mauvaise note en Programmation qu'en Bases de données :
```
SELECT DISTINCT nom_etu
FROM etudiants
RIGHT JOIN notes ON _num_etu = num_etu
RIGHT JOIN matieres ON _num_mat = num_mat
WHERE (matieres.nom_mat = 'Programmation') < (matieres.nom_mat ='Bases de donn%es');
```

7. Nom et numéro des étudiants n'ayant eu aucune note :
```
SELECT DISTINCT nom_etu, 
	num_etu
FROM etudiants
RIGHT JOIN notes ON _num_etu = num_etu
WHERE note IS NULL;
```

<br>
<hr>
<br>

## 3. Regroupements


1. Grades différents existant dans la table des enseignants :
```
SELECT DISTINCT grade
FROM enseignants;
```

2. Par sexe, afficher les différents âges (en années) représentés parmi les étudiants :
```
SELECT sexe, 
	YEAR(CURDATE()) - YEAR(date_naiss) AS 'Age'
FROM etudiants
ORDER BY sexe;
``` 

3. Nombre total d'étudiants :
```
SELECT COUNT(*)
FROM etudiants;
```

4. Date de naissance de l'étudiant le plus jeune et de celui le plus âgé :
```
SELECT MIN(date_naiss) AS 'DDN MIN', 
	MAX(date_naiss) AS 'DDN MAX'
FROM etudiants;
```

5. Pour chaque matière identifiée par son numéro, nombre d'étudiants qui ont une note :
```
SELECT _num_mat, 
	COUNT(note) AS 'Nombre de notes'
FROM notes
WHERE note IS NOT NULL
GROUP BY _num_mat;
```

6. Pour chaque étudiant identifié par son numéro, moyenne obtenue (avec 2 décimales) :
```
SELECT _num_etu,
	-- ROUND prend deux arguments : le nombre à arrondir, et le nombre de décimales de l'arrondi --
	ROUND(AVG(note), 2) AS Moyenne
FROM notes
GROUP BY _num_etu;
```

7. Numéro des étudiants n'ayant eu que 4 notes ou moins :
```
SELECT _num_etu
FROM notes
GROUP BY _num_etu
-- HAVING fonctionne comme WHERE mais permet d'utiliser, pour filtrer les résultats, une fonction d'agréagation (e.g COUNT, SUM...) --
-- HAVING s'utilise après GROUP BY --
HAVING COUNT(note) <= 4;
```

<br>
<hr>
<br>

## Bonus

1. Noms des matières (et de leur enseignant) pour lesquelles la moyenne (non coefficientée) des notes est inférieure à 10 :

2. Pour chaque étudiant ayant eu une note dans chacune des 5 matières, le nom (par ordre alphabétique), le numéro et la moyenne coefficientée obtenue :

3. Nom des enseignants ayant le même grade que Bertrand, Pierre :

4. Pour chaque étudiant, nom et nombre d'étudiants se trouvant avant lui sur la liste alphabétique des noms :


