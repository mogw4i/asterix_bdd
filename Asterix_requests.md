# Requêtes de la BDD Astérix

1. Liste des potions : Numéro, libellé, formule et constituant principal :
```
SELECT * 
FROM potion;
```

2. Liste des noms des trophées rapportant 3 points :
```
-- les trophées n'ont pas de nom, mais leur catégorie si --
-- on utilise la clause DISTINCT afin d'éviter la redondance dans les résultats --
SELECT DISTINCT NomCateg
FROM categorie
WHERE categorie.NbPoints = 3;
```

3. Liste des villages (noms) contenant (strictement) plus de 35 huttes :
```
SELECT NomVillage
FROM village
WHERE NbHuttes > 35;
```

4. Liste des trophées (numéros) pris en mai / juin 52 :
```
SELECT NumTrophee 
FROM trophee
-- format date : 'AAAA-MM-JJ' --
-- BETWEEN permet d'évaluer un intervalle (inclus) --
WHERE DatePrise BETWEEN '2052-05-01'
AND '2052-06-30';
``` 

5. Noms des habitants commençant par 'a' et contenant la lettre 'r' :  
```
SELECT Nom
FROM habitant
-- utilisation de la clause LIKE pour évaluer une chaîne --
-- je ne sais pas comment demander à ce que la lettre r soit présente quelque part dans une regex --
WHERE Nom LIKE 'A%r%';
```

6. Numéros des habitants ayant bu les potions numéros 1, 3 ou 4 :
```
-- On va devoir lier les tables : absorber et habitant --
SELECT DISTINCT habitant.NumHab
FROM habitant
RIGHT JOIN absorber ON absorber.NumHab = habitant.NumHab
-- la syntaxe IN (...) permet d'éviter de réécrire plusieurs fois des conditions avec OR --
WHERE absorber.NumPotion IN (1, 3, 4); 
```

7. Liste des trophées : numéro, date de prise, nom de la catégorie et nom du preneur :  
```
-- on va lier les tables : trophee, habitant et categorie --
SELECT NumTrophee, 
    DatePrise, 
    NomCateg, 
    Nom
FROM habitant
-- RIGHT JOIN car les habitants n'ont pas forcément pris de trophée --
RIGHT JOIN trophee ON trophee.NumPreneur = habitant.NumHab
-- LEFT JOIN car une catégorie n'a pas forcément un trophée associé --
LEFT JOIN categorie ON trophee.CodeCat = categorie.CodeCat;
```

8. Nom des habitants qui habitent à Aquilona :  
```
SELECT Nom
FROM habitant
LEFT JOIN village ON village.NumVillage = habitant.NumVillage
WHERE NomVillage = 'Aquilona';
```

9. Nom des habitants ayant pris des trophées de catégorie Bouclier de Légat :  
```
SELECT DISTINCT Nom
FROM habitant
RIGHT JOIN trophee ON NumPreneur = habitant.NumHab
INNER JOIN categorie ON categorie.CodeCat = trophee.CodeCat
WHERE NomCateg = "Bouclier de Légat";
```


10. Liste des potions (libellés) fabriquées par Panoramix : libellé, formule et constituant principal :
``` 
SELECT LibPotion, 
    Formule, 
    ConstituantPrincipal
FROM potion
INNER JOIN fabriquer ON fabriquer.NumPotion = potion.NumPotion
RIGHT JOIN habitant ON habitant.NumHab = fabriquer.NumHab
WHERE Nom = 'Panoramix';
``` 

11. Liste des potions (libellés)  absorbées par Homéopatix :
```   
SELECT LibPotion
FROM potion
RIGHT JOIN absorber ON absorber.NumPotion = potion.NumPotion
RIGHT JOIN habitant ON absorber.NumHab = habitant.NumHab
WHERE Nom = 'Homéopatix';
```
 

12. Liste des habitants (noms) ayant absorbé une potion fabriquée par l'habitant numéro 3 :
```  
-- DISTINCT car un habitant a peut-être consommé une potion de l'habitant n°3 plusieurs fois --
SELECT DISTINCT Nom
FROM habitant
-- sélectionne tous les habitants ayant absorbé une potion
RIGHT JOIN absorber ON absorber.NumHab = habitant.NumHab
RIGHT JOIN potion ON potion.NumPotion = absorber.NumPotion
-- on va précise qui a fabriqué la potion --
INNER JOIN fabriquer ON fabriquer.NumPotion = potion.NumPotion
WHERE fabriquer.NumHab = 3;
```
 

13. Liste des habitants (noms) ayant absorbé une potion fabriquée par Amnésix :
``` 
-- des habitants ont peut-être consommé plusieurs fois des potions d'Amnésix --
SELECT DISTINCT Nom
FROM habitant
RIGHT JOIN absorber ON absorber.NumHab = habitant.NumHab
RIGHT JOIN potion ON potion.NumPotion = absorber.NumPotion
RIGHT JOIN fabriquer ON fabriquer.NumPotion = potion.NumPotion
-- on va créer un sous-requête afin de sélectionner le numéro d'Amnésix --
WHERE fabriquer.NumHab = (
    SELECT DISTINCT habitant.NumHab
    FROM habitant
    RIGHT JOIN fabriquer ON fabriquer.NumHab = habitant.NumHab
    WHERE Nom = 'Amnésix'
);
```

14. Nom des habitants dont la qualité n'est pas renseignée :  
```
SELECT Nom
FROM habitant
-- IS NULL sélectionne ce qui est vide (NULL) --
WHERE habitant.NumQualite IS NULL;
```
 

15. Nom des habitants ayant consommé la Potion magique n°1 (c'est le libellé de la potion) en février 52 :  
```
SELECT DISTINCT Nom
FROM habitant
RIGHT JOIN absorber ON absorber.NumHab = habitant.NumHab
RIGHT JOIN potion ON absorber.NumPotion = potion.NumPotion
-- je ne sais pas si 2052 est une année bissextile, mais dans le doute, on va dire que oui --
-- on peut mettre la wildcard dans une date --
WHERE DateA LIKE '2052-02-%'
AND LibPotion = "Potion magique n°1";
```

16. Nom et âge des habitants par ordre alphabétique :  
```
SELECT Nom, 
    Age
FROM habitant
-- ORDER BY va trier les nombres en ordre croissant, et les chaînes par ordre alphabétique --
ORDER BY Nom;
```
 

17. Liste des resserres classées de la plus grande à la plus petite : nom de resserre et nom du village :
```  
SELECT NomResserre, 
    NomVillage
-- on peut sélectionner plusieurs tables en même temps --
FROM village, 
    resserre
-- mais pour n'afficher que ce l'on souhaite, on met une condition --
WHERE village.NumVillage = resserre.NumVillage
-- on trie par ordre croissant de la superficie de la resserre --
ORDER BY Superficie;
```

18. Nombre d'habitants du village numéro 5 :  
```
-- La fonction d'agrégation (= qui calcule un seul résultat à partir de plusieurs lignes en entrée) --
-- COUNT() retourne le nombre d'enregistrements répondant aux critères demandés --
-- ici, je sélectionne indifféremment toutes les colonnes de la table habitant  --
SELECT COUNT(*)
FROM habitant
-- mais ici, je spécifie que je veux que le numéro du village soit 5 --
WHERE NumVillage = 5;
```
 

19. Nombre de points gagnés par Goudurix :
``` 
-- La fonction d'agréagiont SUM() retourne la somme des valeurs des enregistrements de la colonne passée en argument --
-- En effet, Goudurix a peut-être gagné plusieurs trophées, donc a peut-être accumulé des points --
SELECT SUM(NbPoints)
FROM categorie
RIGHT JOIN trophee ON trophee.CodeCat = categorie.CodeCat
RIGHT JOIN habitant ON habitant.NumHab = trophee.NumPreneur
-- on précise qui est l'habitant que l'on recherche --
WHERE Nom = 'Goudurix';
```

20. Date de première prise de trophée : 
```
-- La fonction d'agrégation MIN() retourne la plus petite valeur des enregistrements de la colonne passée en argument --
SELECT MIN(DatePrise) 
FROM trophee;
```

21. Nombre de louches de potion magique n°2 (c'est le libellé de la potion) absorbées :
```
SELECT SUM(Quantite)
FROM absorber
RIGHT JOIN potion ON potion.NumPotion = absorber.NumPotion
-- c'est complétement con de faire ça, mais ça me retourne une erreur suédoise sinon -_- --
WHERE LibPotion IN (
    SELECT LibPotion
    FROM potion
    WHERE LibPotion = 'Potion magique n°2'
);
```

22. Superficie la plus grande : 
```
-- il n'y a que les resserres qui ont une superficie dans la BDD --
-- MAX() est une fonction d'agrégation qui retourne la valeur la plus grande des enregistrements de la colonne passée en argument --
SELECT MAX(Superficie)
FROM resserre;
```

23. Nombre d'habitants par village (nom du village, nombre) : 
```
SELECT NomVillage, 
    COUNT(habitant.NumVillage) AS NbHabVillage
FROM village, 
    habitant
WHERE habitant.NumVillage = village.NumVillage
-- GROUP BY est une clause prenant soit le numéro de la colonne sélectionnée ou son nom --
-- et va permettre de retourner le réqultat de la requête selon cette colonne 
-- si on ne groupe pas les résultat -> erreur --
-- GROUP BY s'applique toujours après la clause WHERE --
GROUP BY 1;
```

24. Nombre de trophées par habitant :
```
-- on va séléctionner les résultats comme suit : nom habitant | nombre de trophées --
SELECT Nom, 
    COUNT(*)
FROM habitant, 
    trophee
WHERE habitant.NumHab = trophee.NumPreneur
-- a priori, on a demandé à trier par habitant --
GROUP BY 1;
```

25. Moyenne d'âge des habitants par province (nom de province, calcul) : 
```
-- La fonction d'agrégation AVG() retourne la moyenne des valeurs des enregistrements de la colonne passée en argument --
SELECT NomProvince,
-- FLOOR pour arrondir au plus petit -- 
    FLOOR(AVG(Age)) AS MoyenneDAge
FROM province, 
    habitant
INNER JOIN village ON habitant.NumVillage = village.NumVillage
WHERE village.NumProvince = province.NumProvince
GROUP BY NomProvince;
```

26. Nombre de potions différentes absorbées par chaque habitant (nom de l'habitant et nombre de potions) :
```
-- <=> Total de potions distinctes absorbées par chaque habitant => --
-- On utilise DISTINCT dans le COUNT afin de ne pas compter plusieurs fois une même potion, si un habitant l'a consommée plusieurs fois --
-- "*" ne fonctionne pas dans COUNT avec la clause DISTINCT ; j'ai choisi donc de compter les rangées de numéro de la potion (choix arbitraire) --
-- on peut créer un alias à partir d'une fonction d'agrégation --
SELECT Nom, 
    COUNT(DISTINCT NumPotion) AS NbPotions
FROM habitant, 
    absorber
WHERE absorber.NumHab = habitant.NumHab
GROUP BY Nom;
```

27. Nom des habitants ayant bu plus de 2 louches de potion zen :
``` 
SELECT Nom
FROM habitant
RIGHT JOIN absorber ON absorber.NumHab = habitant.NumHab
RIGHT JOIN potion ON potion.NumPotion = absorber.NumPotion
WHERE LibPotion = 'Potion Zen'
AND Quantite > 2;
``` 

28. Noms des villages dans lesquels on trouve une reserre : 
```
SELECT NomVillage
FROM village
RIGHT JOIN resserre ON resserre.NumVillage = village.NumVillage;
```

29. Nom du village contenant le plus grand nombre de huttes : 
```
SELECT NomVillage
FROM village
WHERE NbHuttes = (
    SELECT MAX(NbHuttes)
    FROM village
);
```

30. Noms des habitants ayant pris plus de trophées qu'Obélix : 
```
SELECT DISTINCT Nom
FROM habitant
RIGHT JOIN trophee ON NumPreneur = NumHab
GROUP BY Nom
HAVING COUNT(NumTrophee) > (
    -- ici, on va calculer le nombre de trophées obtenus par Obélix --
    SELECT COUNT(NumTrophee)
    FROM trophee
    LEFT JOIN habitant ON NumPreneur = NumHab
    WHERE Nom = 'Obélix'
);
```